
import Auth from "pages/auth";
import Tasks from "pages/tasks"


const privateRoutes = [
  {
    Component: Tasks,
    path: '/',
    title: 'Task Management',
  },
]

const publicRoutes = [
  {
    Component: Auth,
    path: '/',
    title: 'Auth',
  },

]

export { publicRoutes, privateRoutes }
