import React from "react";
import { useSelector } from "react-redux";
import { Navigate, Route, Routes } from "react-router-dom";
import { privateRoutes, publicRoutes } from "./allRoutes";

function AuthRoute() {
  const user = useSelector((state: any) => state.root.user.user);

  return (
    <>
      <Routes>
        {!user
          ? publicRoutes.map((item, inx) => {
              return (
                <React.Fragment key={inx}>
                  <Route path={item.path} key={inx} element={<item.Component />} />
                  <Route path={"*"} element={<Navigate to="/" replace />} />
                </React.Fragment>
              );
            })
          : privateRoutes.map((item, inx) => {
              return (
                <React.Fragment key={inx}>
                  <Route path={item.path} key={inx} element={<item.Component />} />
                  <Route path={"*"} element={<Navigate to="/" replace />} />
                </React.Fragment>
              );
            })}
      </Routes>
    </>
  );
}

export default AuthRoute;
