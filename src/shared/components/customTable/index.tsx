import React, { useState, useEffect } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { tableStyles } from "./styles";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import moment from "moment";
import { NoData } from "assets";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import UnfoldMoreIcon from "@mui/icons-material/UnfoldMore";
import Pagination from "@mui/material/Pagination";

interface Task {
  id: number;
  title: string;
  assignee: { name: string };
  deadline: string;
  priority: string;
  description: string;
  status: boolean;
}

interface CustomTableProps {
  data: Task[];
  handleOpenEdit: (item: Task) => void;
  handleDelete: (itemId: number) => void;
  handleCompleteTask: (itemId: number) => void;
}

export default function CustomTable({ data, handleOpenEdit, handleDelete, handleCompleteTask }: CustomTableProps) {
  const classes = tableStyles();
  const [dataBody, setDataBody] = useState<Task[]>([]);
  const [currentPage, setCurrentPage] = useState(0);
  const rowsPerPage = 5;

  // Handle search based on title and assignee name
  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value.toLowerCase();
    const temp = data.filter((item: Task) => item.title.toLowerCase().includes(value) || item.assignee?.name.toLowerCase().includes(value));
    setDataBody(temp);
  };

  // Handle priority filter
  const handleFilter = () => {
    setDataBody((prevData: Task[]) => {
      return [...prevData].sort((a: Task, b: Task) => {
        // Define priority order based on "High", "Medium", and "Low"
        const priorityOrder = ["High", "Medium", "Low"];
        // Get the index of priority for each item
        const priorityA = priorityOrder.indexOf(a.priority);
        const priorityB = priorityOrder.indexOf(b.priority);
        // Compare the priority index and return the appropriate value for sorting
        return priorityA - priorityB;
      });
    });
  };

  useEffect(() => {
    setDataBody(data);
  }, [data]);

  // Function to handle page changes
  const handleChangePage = (event: React.ChangeEvent<unknown>, newPage: number) => {
    setCurrentPage(newPage - 1);
  };

  return data?.length ? (
    <>
      {/* Search input */}
      <input className={classes.search} placeholder="Search by title and assignee" onChange={handleSearch} />
      <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          {/* Table header */}
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Title</TableCell>
              <TableCell>Assignee</TableCell>
              <TableCell>Deadline</TableCell>
              <TableCell sx={{ display: "flex", cursor: "pointer" }}>
                Priority <UnfoldMoreIcon onClick={() => handleFilter()} />{" "}
              </TableCell>
              <TableCell>Description</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {dataBody.slice(currentPage * rowsPerPage, currentPage * rowsPerPage + rowsPerPage).map((item: any) => (
              <TableRow key={item.id}>
                {/* Task completion status */}
                <TableCell>
                  <div onClick={() => handleCompleteTask(item.id)}>{!item?.status ? <CheckCircleOutlineIcon style={{ cursor: "pointer" }} /> : <CheckCircleIcon style={{ cursor: "pointer", color: "green" }} />}</div>
                </TableCell>
                {/* Task details */}
                <TableCell className={classes.tableRow}>{item?.title}</TableCell>
                <TableCell className={classes.tableRow}>
                  <div style={{ display: "flex", alignItems: "center" }}>
                    <img src={item?.assignee?.image} alt={"employee"} className={classes.imgClass} />
                    {item?.assignee?.name}
                  </div>
                </TableCell>
                <TableCell className={classes.tableRow}>{moment(item?.deadline).format("MMMM Do YYYY")}</TableCell>
                <TableCell className={classes.tableRow}>
                  <div className={item?.priority == "High" ? classes.highPriority : item?.priority == "Low" ? classes.LowPriority : classes.MediumPriority}>{item?.priority}</div>
                </TableCell>
                <TableCell className={classes.tableRow}>{item?.description.length > 10 ? `${item?.description.slice(0, 10)}...` : item?.description}</TableCell>
                {/* Task actions */}
                <TableCell className={classes.tableRow}>
                  <div style={{ display: "flex" }}>
                    <EditIcon sx={{ cursor: "pointer", paddingRight: "10px", color: "#435ebe" }} onClick={() => handleOpenEdit(item)} />
                    <DeleteIcon sx={{ cursor: "pointer", color: "red" }} onClick={() => handleDelete(item.id)} />
                  </div>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      {/* Pagination */}
      <Pagination
        count={Math.ceil(dataBody.length / rowsPerPage)}
        page={currentPage + 1}
        onChange={handleChangePage}
        variant="outlined"
        shape="rounded"
        className={classes.Pagination}
        sx={{
          "& .MuiPaginationItem-root": {
            height: "50px",
            width: "50px",
            fontSize: "1rem",
          },
          "& .Mui-selected": {
            backgroundColor: "#435ebe !important",
            color: "white",
            border: "none",
          },
        }}
      />
    </>
  ) : (
    // Show "No Data" image when data is empty
    <div style={{ width: "100%", display: "flex", justifyContent: "center" }}>
      <img src={NoData} style={{ height: "350px", width: "350px" }} alt="No Data" />
    </div>
  );
}
