import { makeStyles } from "@mui/styles";

const tableStyles = makeStyles(() => ({
  tableHead: {
    // background: "#dddddd",
  },
  tableRow: {

  },
  search: {
    width: "99%",
    border: "none",
    borderRadius: "5px",
    height: "40px",
    paddingLeft: "10px",
    marginBottom: "20px",
    "&:focus-visible": {
      outline: "none",
    },
  },
  Pagination: {
    display: "flex",
    justifyContent: "center",
    marginTop: "20px"
  },
  highPriority: {
    width: '60px',
    background: 'red',
    padding: '3px',
    display: 'flex',
    justifyContent: 'center',
    color: 'white',
    borderRadius: '5px'
  },
  LowPriority: {
    width: '60px',
    background: 'yellow',
    padding: '3px',
    display: 'flex',
    justifyContent: 'center',
    color: 'black',
    borderRadius: '5px'
  },
  MediumPriority: {
    width: '60px',
    background: 'green',
    padding: '3px',
    display: 'flex',
    justifyContent: 'center',
    color: 'white',
    borderRadius: '5px'
  },
  imgClass: {
    marginRight: "10px",
    width: "30px",
    height: "30px",
    borderRadius: "50%"
  },
}));

export { tableStyles };
