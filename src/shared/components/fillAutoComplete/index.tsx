import React from 'react'
import CloseIcon from "@mui/icons-material/Close";
import { FillAutoCompleteStyles } from './styles';

interface Employee {
  name: string;
  image: string;
}

interface FillAutoCompleteProps {
  selectedEmployee: Employee | null;
  setSelectedEmployee: React.Dispatch<React.SetStateAction<Employee | null>>;
}

function FillAutoComplete({ selectedEmployee, setSelectedEmployee }: FillAutoCompleteProps) {
  const classes = FillAutoCompleteStyles();

  // Handler for clearing the selected employee
  const handleClearSelectedEmployee = () => {
    setSelectedEmployee(null);
  };

  return (
    <>
      {/* Container to display the selected employee */}
      <div className={classes.selectedImageContainer}>
        <div className={classes.imgContainer}>
          {/* Display the employee image and name */}
          <img src={selectedEmployee?.image} className={classes.imgClass} alt="employee" />
          <div>{selectedEmployee?.name}</div>
        </div>
        {/* Close icon to clear the selected employee */}
        <div>
          <CloseIcon sx={{ cursor: "pointer" }} onClick={handleClearSelectedEmployee} />
        </div>
      </div>
    </>
  );
}

export default FillAutoComplete;
