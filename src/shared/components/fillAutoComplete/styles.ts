import { makeStyles } from "@mui/styles";

const FillAutoCompleteStyles = makeStyles(() => ({
  imgClass: {
    marginRight: "10px",
    width: "30px",
    height: "30px",
    borderRadius: "50%"
  },
  selectedImageContainer: {
    marginTop: "20px",
    display: 'flex',
    alignItems: 'center',
    justifyContent: "space-between",
    border: "1px solid rgba(0, 0, 0, 0.23)",
    padding: "0px 15px",
    borderRadius: "5px"
  },
  imgContainer: {
    display: "flex",
    alignItems: "center",
    height: "56px"
  }
}));

export { FillAutoCompleteStyles };
