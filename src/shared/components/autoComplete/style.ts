import { makeStyles } from "@mui/styles";

const AutoCompleteStyles = makeStyles(() => ({
  imgClass: {
    marginRight: "10px",
    width: "30px",
    height: "30px",
    borderRadius: "50%"
  },
}));

export { AutoCompleteStyles };
