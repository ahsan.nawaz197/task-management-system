import React from "react";
import { Autocomplete, TextField } from "@mui/material";
import { employees } from "shared/utils/data";
import { AutoCompleteStyles } from "./style";



interface AutoCompleteProps {
  searchText: string;
  setSearchText: React.Dispatch<React.SetStateAction<string>>;
  selectedEmployee: any;
  setSelectedEmployee: React.Dispatch<React.SetStateAction<any>>;
}

function AutoCompleteEmployee({
  searchText,
  setSearchText,
  selectedEmployee,
  setSelectedEmployee,
}: AutoCompleteProps) {
  const classes = AutoCompleteStyles();

  // Handle input change in the Autocomplete component
  const handleInputChange = (event: React.SyntheticEvent, newValue: any) => {
    if (typeof newValue !== "object") {
      // Reset selectedEmployee and searchText when the user manually types a value not present in options
      setSelectedEmployee(null);
      setSearchText("");
    } else {
      setSelectedEmployee(newValue);
    }
  };

  return (
    <>
      <Autocomplete
        id="free-solo-demo"
        freeSolo
        sx={{ marginTop: "20px" }}
        options={employees}
        getOptionLabel={(option: any) => option.name}
        onChange={(event, newValue: any | null) => {
          handleInputChange(event, newValue);
          setSearchText("");
        }}
        renderOption={(props, option: any) => (
          <li {...props}>
            {/* Show the employee image and name */}
            <img src={option.image} alt={option.name} className={classes.imgClass} />
            {option.name}
          </li>
        )}
        renderInput={(params) => {
          return (
            <TextField
              {...params}
              placeholder="Select employee"
              value={searchText}
              onChange={(e) => setSearchText?.(e.target.value)}
            />
          );
        }}
      />
    </>
  );
}

export default AutoCompleteEmployee;
