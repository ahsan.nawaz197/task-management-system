import React, { useEffect, useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";
import { Box, TextField, Typography } from "@mui/material";
import { Modal } from "./styles";
import CloseIcon from "@mui/icons-material/Close";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import PrioritySet from "shared/components/priortySet";
import AutoCompleteEmployee from "shared/components/autoComplete";
import FillAutoComplete from "shared/components/fillAutoComplete";
import { toastMessage } from "shared/components/toast";
import moment from "moment";

// Transition component for the dialog
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

interface AddTaskProps {
  show: boolean;
  handleClose: () => void;
  handleClickOpen: () => void;
  handleAdd: any;
  selectedEntry: any;
  setSelectedEntry: any;
}

export default function AlertDialogSlide({ show, handleClose, handleClickOpen, handleAdd, selectedEntry, setSelectedEntry }: AddTaskProps) {
  const [searchText, setSearchText] = useState<string>("");
  const [title, setTitle] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [selectedEmployee, setSelectedEmployee] = useState<any>(null);
  const [selectedDate, setSelectedDate] = useState<Date>(new Date());
  const [priority, setPriority] = useState<string>("");
  const classes = Modal();

  // Reset form fields and close the modal
  const onClose = () => {
    setSelectedEmployee(null);
    handleClose();
    setSelectedDate(new Date());
    setPriority("");
    setDescription("");
    setTitle("");
    setSelectedEntry("");
  };

  // Handle form submission
  const handleSubmit = () => {
    if (!title?.length || !description?.length || !selectedEmployee || !priority?.length || !selectedDate) {
      toastMessage("Please enter all entries", "error");
    } else {
      const task = {
        id: selectedEntry ? selectedEntry?.id : Math.floor(Math.random() * 1000),
        title,
        description,
        assignee: selectedEmployee,
        deadline: selectedDate,
        priority,
        status: selectedEntry?.status || false,
      };

      handleAdd(task, !!selectedEntry);
      onClose();
    }
  };

  // Set form fields when an existing task is edited
  useEffect(() => {
    if (selectedEntry) {
      setTitle(selectedEntry?.title);
      setDescription(selectedEntry?.description);
      setSelectedEmployee(selectedEntry?.assignee);
      setSelectedDate(new Date(selectedEntry?.deadline));
      setPriority(selectedEntry?.priority);
    }
  }, [selectedEntry]);

  return (
    <div>
      <Dialog
        open={show}
        TransitionComponent={Transition}
        keepMounted
        onClose={onClose}
        aria-describedby="alert-dialog-slide-description"
        PaperProps={{
          classes: {
            root: classes.modalContainer,
          },
        }}
      >
        <DialogContent sx={{ position: "relative" }}>
          <CloseIcon className={classes.closeIcon} onClick={handleClose} />
          <Typography variant="h5" className={classes.title}>
            Add Task
          </Typography>
          <Box sx={{ paddingTop: "20px" }}>
            {/* Title field */}
            <TextField id="outlined-basic" label="Title" variant="outlined" placeholder="Enter title" sx={{ width: "100%", marginTop: "20px" }} value={title} onChange={(e) => setTitle(e.target.value)} />
            {/* Description field */}
            <TextField placeholder="Description" multiline rows={2} maxRows={4} sx={{ width: "100%", paddingTop: "20px" }} value={description} onChange={(e) => setDescription(e.target.value)} />
            {/* AutoCompleteEmployee component */}
            {selectedEmployee ? <FillAutoComplete selectedEmployee={selectedEmployee} setSelectedEmployee={setSelectedEmployee} /> : <AutoCompleteEmployee searchText={searchText} setSearchText={setSearchText} selectedEmployee={selectedEmployee} setSelectedEmployee={setSelectedEmployee} />}
            {/* DatePicker component */}
            <DatePicker selected={selectedDate} wrapperClassName="datePicker" onChange={(date: any) => setSelectedDate(date)} dateFormat="dd/MM/yyyy" placeholderText="Select a date" minDate={new Date()} />
            {/* PrioritySet component */}
            <PrioritySet priority={priority} setPriority={setPriority} />
          </Box>
          <Box>
            {/* Submit button */}
            <Button className={classes.submitButton} variant="contained" onClickCapture={() => handleSubmit()}>
              {selectedEntry ? "Update" : "Add"}
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
    </div>
  );
}
