import { makeStyles } from "@mui/styles";

const Modal = makeStyles(() => ({
  title: {
    fontFamily: "Montserrat !important",
    fontWeight: 700,
  },
  modalContainer: {
    width: "100%",
  },
  submitButton: {
    width: "100%",
    background: "#435ebe !important",
    color: "white !important",
    height: "56px",
    marginTop: "20px !important",
  },
  closeIcon: {
    position: "absolute",
    top: "24px",
    right: "24px",
    cursor: "pointer",
  }
}));

export { Modal };
