import { makeStyles } from "@mui/styles";

const PrioritySelection = makeStyles(() => ({
  container: {
    marginTop: "20px"
  }
}));

export { PrioritySelection };
