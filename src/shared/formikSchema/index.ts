import * as Yup from "yup";


const SignupSchema = Yup.object().shape({
  name: Yup.string().min(2, "Too Short!").max(50, "Too Long!").required("Project name is required"),
});

export {
  SignupSchema
}