import { Amy, Charles, Gina, Jake, Raymond, Rosa, Terry } from "assets"

let employees = [
  {
    id: 1,
    name: "Jake",
    designation: "Detective",
    image: Jake
  },
  {
    id: 2,
    name: "Amy",
    designation: "Detective",
    image: Amy
  },
  {
    id: 3,
    name: "Charles",
    designation: "Detective",
    image: Charles
  },
  {
    id: 4,
    name: "Rosa",
    designation: "Detective",
    image: Rosa
  },
  {
    id: 5,
    name: "Raymond",
    designation: "Captain",
    image: Raymond
  },
  {
    id: 6,
    name: "Terry",
    designation: "Sargent",
    image: Terry
  },
  {
    id: 7,
    name: "Gina",
    designation: "Assistant",
    image: Gina
  },
]

export {
  employees
}