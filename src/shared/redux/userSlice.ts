import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// state type
export interface UserState {
  user: any; 
}

// initial state
const initialState: UserState = {
  user: null,
};

// slicer
const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    login: (state, action: PayloadAction<any>) => {
      state.user = action.payload;
    },
    logout: (state, action: PayloadAction<any | null>) => {
      state.user = action.payload || null;
    },
  },
});

// actions
export const { login, logout } = userSlice.actions;

// reducer
export default userSlice.reducer;
