import { makeStyles } from "@mui/styles";

const taskListing = makeStyles(() => ({
  button: {
    background: "#435ebe !important",
  },
  root: {
    padding: "50px 70px",
    background: "#f2f7ff!important",
    minHeight: "100vh",
    "@media (max-width: 700px)": {
      padding: "30px",
    },
  },
  title: {
    fontFamily: "Montserrat !important",
    fontWeight: 700,
  },
  nameContainer: {
    padding: "20px 5px",
    display: "flex",
    justifyContent: "space-between"
  },

}));

export { taskListing };
