import React, { useState } from "react";
import CustomTable from "shared/components/customTable";
import { taskListing } from "./styles";
import { Box, Button, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import AddTask from "shared/components/modals/AddTask";
import { login, logout } from "shared/redux/userSlice";
import { toastMessage } from "shared/components/toast";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";

function Task() {
  const dispatch = useDispatch();
  const classes = taskListing();
  
  // Get user data from Redux store
  const user = useSelector((state: any) => state?.root?.user?.user);
  
  // Local state to manage selected entry, modal open state, and task data
  const [selectedEntry, setSelectedEntry] = useState<any>(null);
  const [open, setOpen] = useState<boolean>(false);
  const [data, setData] = useState<any>(user?.data?.length ? user?.data : []);

  // Open the add task modal
  const handleClickOpen = () => {
    setOpen(true);
  };

  // Close the add task modal
  const handleClose = () => {
    setOpen(false);
  };

  // Add or update task based on the 'update' flag
  const handleAdd = (entry: any, update: boolean) => {
    if (update) {
      // Update task
      let temp = [...data];
      let index = temp?.findIndex((item: any) => item?.id === entry?.id);
      if (index > -1) {
        temp[index] = entry;
      }
      setData(temp);
      setLocalStorage(temp);
      toastMessage("Task updated", "success");
    } else {
      // Add new task
      let temp = [entry, ...data];
      setData(temp);
      setLocalStorage(temp);
      toastMessage("Task added", "success");
    }
  };

  // Open the edit task modal
  const handleOpenEdit = (entry: any) => {
    setSelectedEntry(entry);
    handleClickOpen();
  };

  // Delete a task
  const handleDelete = (entryId: number) => {
    let temp = [...data];
    let index = temp?.findIndex((item: any) => item?.id === entryId);
    if (index > -1) {
      temp.splice(index, 1);
    }
    setData(temp);
    setLocalStorage(temp);
    toastMessage("Task deleted", "success");
  };

  // Mark task as completed or back to queue
  const handleCompleteTask = (entryId: number) => {
    let temp = data.map((item: any) => {
      if (item.id === entryId) {
        if (!item.status) {
          toastMessage("Task completed", "success");
        } else {
          toastMessage("Task back in the queue", "success");
        }
        return { ...item, status: !item.status };
      }
      return item;
    });

    setData(temp);
    setLocalStorage(temp);
  };

  // Save data to local storage and update Redux store
  const setLocalStorage = (data: any) => {
    let obj = {
      projectName: user?.projectName,
      data,
    };
    dispatch(login(obj));
  };

  // Handle project completion
  const handleProject = () => {
    toastMessage("Project Completed", "success");
    dispatch(logout(null));
  };

  return (
    <div className={classes.root}>
      <Box className={classes.nameContainer}>
        <Typography variant="h5" className={classes.title}>
          <Box sx={{ display: "flex", alignItems: "center" }}>
            {/* Show project name with a check icon for completion */}
            {user?.projectName}{" "}
            <CheckCircleOutlineIcon
              style={{ marginLeft: "10px", cursor: "pointer" }}
              onClick={() => handleProject()}
            />
          </Box>
        </Typography>
        {/* Button to add a new task */}
        <Button variant="contained" className={classes.button} onClick={handleClickOpen}>
          Add Task
        </Button>
      </Box>
      {/* Display the task table */}
      <CustomTable
        data={data}
        handleOpenEdit={handleOpenEdit}
        handleDelete={handleDelete}
        handleCompleteTask={handleCompleteTask}
      />
      {/* Modal for adding or editing tasks */}
      <AddTask
        show={open}
        handleClickOpen={handleClickOpen}
        handleClose={handleClose}
        handleAdd={handleAdd}
        selectedEntry={selectedEntry}
        setSelectedEntry={setSelectedEntry}
      />
    </div>
  );
}

export default Task;
