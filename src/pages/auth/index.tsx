import React from "react";
import { Card, CardContent, Typography, TextField, Button, Box } from "@mui/material";
import { Formik, Form, Field } from "formik";
import { SignupSchema } from "shared/formikSchema";
import { signInStyles } from "./styles";
import { useDispatch } from "react-redux";
import { login } from "shared/redux/userSlice";
import { toastMessage } from "shared/components/toast";

const Auth = () => {
  // Initialize Redux dispatch and styles
  const dispatch = useDispatch();
  const classes = signInStyles();

  // Handle form submission
  const handleSubmit = (values: any) => {
    let obj = {
      projectName: values.name,
      data: [],
    };
    toastMessage("Project created", "success")
    dispatch(login(obj)); // Dispatch the login action with form values
  };

  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          <Typography variant="h5" className={classes.title}>
            Task Management System
          </Typography>
          <Typography className={classes.subTitle}>Enter your project name to continue</Typography>
          <Box sx={{ paddingTop: "30px" }}>
            {/* Use Formik to handle form state and validation */}
            <Formik
              initialValues={{
                name: "",
              }}
              validationSchema={SignupSchema}
              onSubmit={(values) => {
                handleSubmit(values); // Call the custom handleSubmit function
              }}
            >
              {({ errors, touched }) => (
                <Form>
                  {/* Use Field to handle form fields */}
                  <Field name="name" as={TextField} placeholder="Enter your project name" label="Name" variant="outlined" fullWidth error={errors.name && touched.name} />
                  {/* Show error message if there are errors for the name field */}
                  {errors.name && touched.name ? <div className={classes.error}>{errors.name}</div> : <div className={classes.error}></div>}

                  <Button type="submit" variant="contained" color="primary" className={classes.submitButton}>
                    Submit
                  </Button>
                </Form>
              )}
            </Formik>
          </Box>
        </CardContent>
      </Card>
    </div>
  );
};

export default Auth;
