import { makeStyles } from "@mui/styles";

const signInStyles = makeStyles(() => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    background: "#f2f7ff!important"
  },
  card: {
    padding: "2rem"
  },
  cardContent: {
    padding: "0px !important",
    paddingBottom: "0px !important"
  },
  submitButton: {
    width: "100%",
    backgroundColor: "#435ebe !important",
    minHeight: "50px",
    marginTop: "10px !important",
  },
  title: {
    fontFamily: "Montserrat !important",
    fontWeight: 700,
    textAlign: "center"
  },
  subTitle: {
    color: '#6C6C6C',
    fontSize: '14px',
    fontStyle: 'normal',
    lineHeight: 'normal',
    textAlign: "center",
    paddingTop: "10px"
  },
  error: {
    color: "#d32f2f",
    fontWeight: 400,
    fontSize: '0.75rem',
    lineHeight: 1.66,
    letterSpacing: '0.03333em',
    textAlign: 'left',
    height: "20px"
  }
}));

export {
  signInStyles
}