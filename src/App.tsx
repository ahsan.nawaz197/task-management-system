import "./App.css";
import AuthRoute from "shared/routes/authRoutes";
import { ToastContainer } from "react-toastify";

function App() {
  return (
    <>
      <AuthRoute />
      <ToastContainer />
    </>
  );
}

export default App;
