import Jake from "./images/Jake.jpeg"
import Amy from "./images/Amy.jpeg"
import Rosa from "./images/Rosa.jpeg"
import Charles from "./images/Charles.jpg"
import Terry from "./images/Terry.jpeg"
import Raymond from "./images/Raymond.jpeg"
import Gina from "./images/Gina.jpeg";
import NoData from "./images/NoData.jpg";

export {
  Jake,
  Amy,
  Rosa,
  Charles,
  Terry,
  Raymond,
  Gina,
  NoData
}